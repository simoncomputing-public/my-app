import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';

import { Observable } from 'rxjs/Rx';

import { states, roles, titles, User } from '../../user-service/user';
import { UserService } from '../../user-service/user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  states = states;
  roles = roles;
  titles = titles;
  title = 'Add User';

  error = null;

  private userId: string;
  private paramSubscription: ISubscription;

  userForm: FormGroup;

  constructor(private fb: FormBuilder,
    private service: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {      // <- Inject ActivatedRoute
  }

  delayedClear() {
    let timer = Observable.timer(5000);
    timer.subscribe(x => this.error = null);
  }

  saveUser() {

    this.error = this.userId ? this.service.updateUser(this.userForm.value) : this.service.addUser(this.userForm.value);

    if (this.error) {
      this.delayedClear();
      return;
    }

    this.router.navigate(['/userlist']);
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      userId: ['', Validators.required],
      firstName: '',
      lastName: ['', Validators.required],
      role: '',
      title: '',
      active: true,

      address: this.fb.group({
        street: '',
        city: '',
        state: '',
        zip: ''
      }),

    });

    this.paramSubscription = this.activatedRoute.params.subscribe(this.processParams);

  }

  /**
   * Process the array of parameters received on entry into this component.
   * The fat arrow notation is necessary to make sure this. points to the class
   * and not the function that will actually call it.
   *
   * @param params
   */
  processParams = (params) => {
    this.userId = params['userId'];

    console.log("UserID: " + this.userId);
    // if userId is empty, we are in add mode, exit.
    if (!this.userId) return;

    // if userId exists, change the title to indicate we are in update mode
    this.title = `User Update`;
    let user = this.service.getUser(this.userId);

    // if the userId passed in is not found, display na error.
    if (!user) {
      this.error = `User ID="${this.userId}" could not be found.`;
      this.delayedClear();
      return;
    }

    // Initialize the form with the current user's setting
    this.userForm.setValue(user);
  }

  /**
   * unsubscribes from the subscription established in ngOnInit()
   */
  ngOnDestroy() {
    if (this.paramSubscription) this.paramSubscription.unsubscribe();
  }

}
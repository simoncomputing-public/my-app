import { Component, OnInit } from '@angular/core';
import { User } from '../../user-service/user';
import { UserService } from '../../user-service/user.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {

  users: User[]; // <-- add this
  displayedColumns = [ 'userId', 'firstName', 'lastName', 'role', 'title', 'actions' ];
  dataSource: UserDataSource;

  constructor(private service: UserService) { }

  ngOnInit() {
    this.users = this.service.getUsers();
    this.dataSource = new UserDataSource( this.users );
  }

}

export class UserDataSource extends DataSource<any> {
  constructor( private data: User[] ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<User[]> {
    return Observable.of(this.data);
  }

  disconnect() {}
}

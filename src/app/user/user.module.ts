import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserlistComponent } from './userlist/userlist.component';

import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule, MaterialModule, RouterModule, ReactiveFormsModule
  ],
  exports: [UserComponent, UserlistComponent],
  declarations: [UserComponent, UserlistComponent]
})
export class UserModule { }

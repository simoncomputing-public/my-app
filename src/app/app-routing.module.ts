import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user/user.component';
import { UserlistComponent } from './user/userlist/userlist.component';
import {NotFoundComponent } from './tags/not-found/not-found.component';

const routes: Routes = [
  { path: 'update-user/:userId', component: UserComponent },
  { path: 'add-user', component: UserComponent },
  { path: 'userlist', component: UserlistComponent },
  { path: '', redirectTo: 'userlist', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }              // <-- Add this line
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

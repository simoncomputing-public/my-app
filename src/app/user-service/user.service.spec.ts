import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from './user';


// add this interface
interface ThisContext {
  user: User;
  service: UserService;
}

describe('UserService', function() {
  beforeEach( function( this: ThisContext ){
    this.service = new UserService();

    this.user = new User();
    this.user.userId = 'superman';
    this.user.firstName = 'Clark';
    this.user.lastName  = 'Kent';
    this.user.role      = 'PowerUser';
    this.user.title     = 'Project Manager';


  });

  describe('should add user', function(){
    it('when user is null', function(this: ThisContext){
      // act
      const result = this.service.addUser(null);

      // assert
      expect(result).toBeTruthy();
    });

    it('when userId is null', function(this: ThisContext){
      // arrange
      this.user.userId = null;

      // act
      const result = this.service.addUser(this.user);

      // assert
      expect(result).toBeTruthy();
    });

    it('when all is good', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;

      // act
      const result = this.service.addUser(this.user);
      const retrieved = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(retrieved).toBeTruthy();
      expect(retrieved.userId).toEqual(userId);
      expect(retrieved.firstName).toEqual(this.user.firstName);
      expect(retrieved.lastName).toEqual(this.user.lastName);
      expect(retrieved.role).toEqual(this.user.role);
      expect(retrieved.title).toEqual(this.user.title);
    });

    it('when already exists, fails', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      // pre-condition assert
      expect(this.service.getUser(userId)).toBeTruthy();

      // act
      const result = this.service.addUser(this.user);

      // assert
      expect(result).toBeTruthy();
      expect(result).toContain('superman already exists');
    });
  });

  describe('should get user', function(){
    it('when user exists', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      // act
      const result = this.service.getUser(userId);

      // assert
      expect(result).toEqual(this.user);
    });

    it('when user does not exist', function(this: ThisContext){

      // act
      const result = this.service.getUser('something non-existent');

      // assert
      expect(result).toBeNull();
    });

    it('safe copy', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      // act
      const result = this.service.getUser(userId);

      // assert
      expect(result).toEqual(this.user);
      expect(result).not.toBe(this.user);
    });
  });

  describe('should update user', function(){

    it('when fields updated', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);

      const user = this.service.getUser(userId);

      // act
      user.firstName = 'updated ' + this.user.firstName;
      user.lastName = 'updated ' + this.user.lastName;
      user.role = 'updated ' + this.user.role;
      user.title = 'updated ' + this.user.title;
      const result = this.service.updateUser(user);
      const updatedUser = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(updatedUser).toBeTruthy();
      expect(updatedUser.firstName).toEqual(user.firstName);
      expect(updatedUser.lastName).toEqual(user.lastName);
      expect(updatedUser.role).toEqual(user.role);
      expect(updatedUser.title).toEqual(user.title);
    });

    it('when user is null', function(this: ThisContext){
      // act
      const result = this.service.updateUser(null);

      // assert
      expect(result).toBeTruthy();
    });

  });

  describe('should delete user', function(){

    it('when user is null', function(this: ThisContext){
      // act
      const result = this.service.deleteUser(null);

      // assert
      expect(result).toBeTruthy();
    });

    it('when user exists', function(this: ThisContext){
      // arrange
      const userId = this.user.userId;
      this.service.addUser(this.user);
      // pre-condition assert
      expect(this.service.getUser(userId)).toEqual(this.user);

      // act
      const result = this.service.deleteUser(userId);
      const exists = this.service.getUser(userId);

      // assert
      expect(result).toBeNull();
      expect(exists).toBeNull();
    });

    it('when user does not exist', function(this: ThisContext){

      // act
      const result = this.service.deleteUser('some non-existent');

      // assert
      expect(result).toBeTruthy();
    });
  });

  describe('should get all users', function(){

    it('when only one user', function(this: ThisContext){
      // arrange
      this.service.addUser(this.user);

      // act
      const result = this.service.getUsers();

      // assert
      expect(result).toBeTruthy();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(this.user);
    });

    it('when no users', function(this: ThisContext){
      // act
      const result = this.service.getUsers();

      // assert
      expect(result).toBeTruthy();
      expect(result.length).toBe(0);
    });

    it('when many users', function(this: ThisContext){
      // arrange
      for (let i = 0; i < 5; i++) {
        const user = new User(this.user);
        user.userId = user.userId + i;
        this.service.addUser(user);
      }

      // act
      const result = this.service.getUsers();

      // assert
      expect(result).toBeTruthy();
      expect(result.length).toBe(5);
    });
  });



});

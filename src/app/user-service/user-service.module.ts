import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './user.service';
import { User, Address, Phone } from './user';

@NgModule({
  imports: [
    CommonModule
  ],
  providers:[UserService],
  declarations: []
})
export class UserServiceModule { }

import { Injectable } from '@angular/core';
import { User, Address, Phone } from './user';

@Injectable()
export class UserService {
  private userList: User[] = [];

  constructor() { }

  /**
   * Adds the specified user to the userList.  If there are any errors,
   * a message is returned.  Otherwise, null is returned.
   * @param user
   */
  addUser( user: User ): string {
    if ( !user ) return 'user is required.';
    if ( !user.userId ) return 'userId is required.';
    if ( !user.lastName ) return 'lastName required';

    // check for duplicate
    if ( this.getUser( user.userId ) )
      return `${user.userId} already exists.`;

    this.userList.push( user );

    return null;
  }

    /**
   * Returns a copy of the specified user.  Returns null if not found.
   *
   * @param userId
   */
  getUser( userId: string ): User {
    const user = this.userList.find( currentUser => currentUser.userId === userId );
    if ( !user ) return null;

    return new User( user );
  }

  /**
   * Updates the existing user entry with the provided user entry.
   * Returns null if there are no errors.
   *
   * @param user
   */
  updateUser( user: User ): string {
    if ( user == null ) return 'null user not allowed.';

    const index = this.userList.findIndex( u => u.userId === user.userId );
    if ( index < 0 ) return `user.userId[${user.userId}] doesn't exist`;

    this.userList[index] = user;
    return null;
  }

  /**
   * Deletes the specified user.  It returns EntryError
   * if not specified.
   *
   * @param userId
   */
  deleteUser( userId ): string {
    if ( !userId ) return 'Valid userId required';

    const index = this.userList.findIndex( u => u.userId === userId );
    if ( index < 0 ) return `user.userId[${userId}] doesn't exist`;

    this.userList.splice( index, 1 );
    return null;
  }

  /**
   * Returns the full user list.
   */
  getUsers(): User[] {
    return this.userList.map( u => new User( u ) );
  }


}

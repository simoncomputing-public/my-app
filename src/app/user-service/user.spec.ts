// user.spec.ts
import { User, Phone, Address } from "./user";

interface ThisContext {
    user: User,
    address: Address,
    phones: Phone[]
}

describe("User Model", function(){

    beforeEach(function(this: ThisContext){
        this.user = new User();
        this.user.userId = 'one';
        this.user.role = 'rolo';
        this.user.lastName = 'lname';
        this.user.firstName = 'fname';
        this.user.title = 'titulo';
        this.user.active = true;

        this.address = new Address();
        this.address.street = "123 Main Street";
        this.address.city = "Anytown";
        this.address.state = "IL";
        this.address.zip = "12345";

        this.phones = [
            new Phone(),
            new Phone()
        ];
        this.phones[0].type = "home";
        this.phones[0].phoneNo = "123-456-7890";
        this.phones[1].type = "cell";
        this.phones[1].phoneNo = "890-567-1234";
    });

    describe("Create User", function(){
        it("when argument is null", function(){
            // act
            let result = new User();

            // assert
            expect(result).toBeTruthy();
            expect(result.userId).toBe('');
            expect(result.role).toBe('');
            expect(result.lastName).toBe('');
            expect(result.firstName).toBe('');
            expect(result.title).toBe('');
            expect(result.active).toBe(false);
            expect(result.phones).toBeFalsy();
            expect(result.address).toBeFalsy();
        });

        describe("when argument is not null", function(){

            it("has no children", function(this: ThisContext){
                // act
                let result = new User(this.user);
                this.user.userId = 'two'; // make an edit

                // assert
                expect(result).toBeTruthy();
                expect(result.userId).toBe('one');
                expect(result.role).toBe('rolo');
                expect(result.lastName).toBe('lname');
                expect(result.firstName).toBe('fname');
                expect(result.title).toBe('titulo');
                expect(result.active).toBe(true);
                expect(result.phones).toBeFalsy();
                expect(result.address).toBeFalsy();
            });

            it("has address", function(this: ThisContext){
                // arrange
                this.user.address = this.address;

                // act
                let result = new User(this.user);
                this.user.address.state = 'VA'; // make an edit to the original;

                // assert
                expect(result.address).toBeTruthy();
                expect(result.address.street).toBe('123 Main Street');
                expect(result.address.city).toBe('Anytown');
                expect(result.address.state).toBe('IL');
                expect(result.address.zip).toBe('12345');
            });

            it("has phones", function(this: ThisContext){
                // arrange
                this.user.phones = this.phones;

                // act
                let result = new User(this.user);
                this.user.phones.push(new Phone()); // make an edit to the original;

                // assert
                expect(result.phones).toBeTruthy();
                expect(result.phones.length).toBe(2);
                expect(result.phones[0]).toEqual(this.phones[0]);
                expect(result.phones[1]).toEqual(this.phones[1]);
            });
        });
    });
});

export class User {
    userId = '';
    role? = '';          // radio buttons - Admin, PowerUser, User, Guest
    lastName = '';
    firstName = '';
    address: Address;
    phones: Phone[];
    title? = '';         // select box: 'Project Manager', 'Analyst', 'Technical Lead',
                        // 'Developer', 'DevOps', 'Tester'

    active = false;     // Checkbox

    constructor( that?: User ) {
      if ( !that ) return;

      this.userId    = that.userId;
      this.role      = that.role;
      this.lastName  = that.lastName;
      this.firstName = that.firstName;
      this.title     = that.title;
      this.active    = that.active;


      if ( that.address )
        this.address = new Address( that.address );

      if ( that.phones )
        this.phones = that.phones.map( phone => new Phone( phone ));

    }
  }

  export class Address {
    street = '';
    city   = '';
    state  = '';
    zip    = '';

    // copy constructor
    constructor( that?: Address ) {
      if ( !that ) return;

      this.street = that.street;
      this.city    = that.city;
      this.state   = that.state;
      this.zip     = that.zip;
    }
  }

  export class Phone {
    type = '';          // home, work, cell
    phoneNo = '';

    // copy constructor
    constructor( that?: Phone ) {
      if ( !that ) return;

      this.type    = that.type;
      this.phoneNo = that.phoneNo;
    }
  }

  // This contains some short lists to support entry screens
  export const states = [
    { code: 'CA', desc: 'California' },
    { code: 'DC', desc: 'District of Columbia' },
    { code: 'FL', desc: 'Florida' },
    { code: 'MD', desc: 'Maryland' },
    { code: 'NY', desc: 'New York' },
    { code: 'VA', desc: 'Virginia' }
];
  export const titles = [ 'Project Manager', 'Analyst', 'Technical Lead',
                          'Developer', 'DevOps', 'Tester' ];
  export const roles  = [ 'Admin', 'PowerUser', 'User', 'Guest'];
